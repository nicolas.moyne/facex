# ASL Decoder ✌️

## Description

This project is the second week project of the CentraleSupelec Coding Weeks. It can decode the sign language from webcam.

## The FaceX team ❤️

* **Ala Kolsi :** membre à Automatants en DD.

* **Aya Khazri :** 21 ans, Tunisie, Toulouse, Genius.

* **Gabrielle Caillaud :** 20 ans, investie dans Magnum, Automatants et Pics.

* **Juliette Kalfleche :** 19 ans, Toulouse, investie à ViaRézo et LinkCS

* **Marc-Antoine Godde :** 20 ans et de Grenoble, membre de ViaRézo.

* **Nicolas Moyne :** Je suis un clermontois de 20 ans. Passioné d'oenologie et de débats je m'investis dans plusieurs associations, notamment Magnum et Symposium. Je participe aussi à l'AS escalade et à la NCV.

## Project goal 
 We want to recognize letters in sign language. We choosed ASL, the American Sign Language

 ![ASL](./Ressources/asl.jpg)

---
## 🔨 Install dependencies

Firstly, you have to install the required dependencies to run the project. Execute the following command:

```
pip install -r requirements.txt
```

## 🧠 **(Optional)** Generate and train the neural network

We will create a `.h5` model file trained with a free dataset on _Kaggle.com_. __You can use the already existing `Data/model.h5` file on the repository to skip that step.__

1) Download the dataset :
    * Create a free account on _Kaggle.com_.
    * Generate an API token under _Account_ > _API_ > _Create New API Token_.
    * Save that token under `~/.kaggle/kaggle.json`
    * Run the following commands :
```
mkdir dataset && cd dataset
kaggle datasets download grassknoted/asl-alphabet
unzip asl-alphabet.zip
```

2) Split the dataset to create a validation dataset :
    * Execute the following script __in `dataset` folder__:

```
python train/gen.py
```

3) Execute the script :
```
python train_ia/train.py --dataset path/to/dataset/asl_alphabet_train/asl_alphabet_train/ --validation path/to/dataset/asl_alphabet_validation/asl_alphabet_validation/ --output path/to/output/model.h5 --epoch 12
```

Available arguments:
* `-d`, `--dataset` DATASET : Path to train dataset with `asl_alphabet_train/asl_alphabet_train/`
* `-v`, `--validation` VALIDATION :Path to validation dataset with `asl_alphabet_validation/asl_alphabet_validation/`
* `-e`, `--epoch` EPOCH : Number of epochs to compute, default is 12
* `-o`, `--output` OUTPUT : Path to output file, default is `./model.h5`


3) Place the `model.h5` file into `Data` folder before running the program.


## ⚙️ Run the script

* Run the program with the following command:

```
python main.py
```

The following windows should appear :

![Main program](./Ressources/main.jpg)

## 🚀 Usage

* The window on the left is the webcam preview with hand detection.
* The window on the right is the hand cropped according to the hand detection.
* The corresponding ASL character is displayed with its probability at the bottom.
* Mode :
    * MANUAL :
        * Press Save each time you want to append the character to the saved text.
        * Press Export to save in the `.txt` file.
    * AUTO :
        * You have to withdraw your hand from the screen each time you want to append the character. It will detect `nothing` and append the character to the saved text.
        * Press Export to save in the `.txt` file.
* Reset Button resets the saved text.

## 🐛 Known issues

* If you get incorrect results, it might be related to the fact that the neural network isn't perfect. The used dataset probably lacks diversity. __Please consider to place your hand in front of a white background.__
* If the program was closed in an incorrect state, the `Data/img.png` might be corrupt. As a result, the program won't start. You'll have to restore that file.
* :warning:&nbsp;In some cases (espcially on Windows), for `path_img` and `model`, you might have to set an absolute path to `Data` folder in `main.py`. Do the same in `utils_cv/process_image.py` at line :
```
crop_img = cv2.imread("Data/blanc.png")
```

---
## 📂 Script organization

```
ASL_DECODER
├── Data
│   ├── img.png
│   ├── blanc.png
│   └── model.h5
├── Legacy
│   └── graph_inter.py
├── Tests
│   ├── test_predict.py
│   └── test_processing.py
├── train_ia
│   ├── gen.py
│   └── train.py
├── utils_cv
│   ├── ecriture.py
│   ├── handtrack.py
│   ├── predict.py
│   ├── process_image.py
│   ├── webcam.py
│   └── write_file.py
├── CONCEPTION.md
├── Organisation.md
├── README.md
├── main.py
├── main_simplifie.py
└── requirements.txt
```
* **Data** : contains the usefull data 
    * **img.png** : contains the image with the sign to recognize
    * **blanc.png** : default white image
    * **model.h5** : contains the neural network
* **legacy** : contains the old main program (our first MVP)
* **Tests** : contains tests
* **train_ia** : contains the program to generate and train the neural network
* **utils_cv** : contains all the utils functions 
    * **ecriture** : writes into a txt file or a docx file
    * **handtrack** : adds the handtrack feature
    * **predict** : uses the neural network to predict a letter
    * **process_image** : utils to work on images (crop, size, ...)
    * **webcam** : gets images out of the webcam
    * **write_file** : writes in txt or docs file
* **main** : main program
* **main_simplifie** : lighter version of the main program
* **requirements** : recquired packages

