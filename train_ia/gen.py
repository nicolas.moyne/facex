import os

if not os.path.exists('asl_alphabet_validation'):
    os.makedirs('asl_alphabet_validation')

os.chdir('asl_alphabet_validation')

if not os.path.exists('asl_alphabet_validation'):
    os.makedirs('asl_alphabet_validation')

os.chdir('asl_alphabet_validation')

folders = next(os.walk('../../asl_alphabet_train/asl_alphabet_train'))[1]
for folder in folders:
    if not os.path.exists(folder):
        os.mkdir(folder)

for folder in folders:
    for id in range(1,301):
        print(folder+str(id))
        os.rename('../../asl_alphabet_train/asl_alphabet_train/'+folder+"/"+folder+str(id)+".jpg", "./"+folder+"/"+folder+str(id)+".jpg")