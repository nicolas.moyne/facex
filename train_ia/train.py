#!/usr/bin/env python
import argparse
from keras import callbacks
from keras.models import Sequential
from keras.applications.vgg16 import VGG16
from keras.layers.core import Flatten, Dense
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator

parser = argparse.ArgumentParser(description="ASL neural network generator")

parser.add_argument("-d", "--dataset", help="Path to train dataset", required=True)
parser.add_argument("-v", "--validation", help="Path to validation dataset", required=True)
parser.add_argument("-e", "--epoch", help="Number of epochs to compute", required=True, default='12')
parser.add_argument("-o", "--output", help="Path to output file", required=True)

args = parser.parse_args()

if __name__ == '__main__':

    train_directory = args.dataset
    validation_directory = args.validation

    batch = 256
    classes = 29
    epochs = int(args.epoch)
    lnr = 0.0001

    gen = ImageDataGenerator(rescale=1./255, validation_split=0.1) 
    train = gen.flow_from_directory(train_directory, target_size=(64, 64), subset="training")
    val = gen.flow_from_directory(validation_directory, target_size=(64, 64), subset="validation")

    adam = Adam(learning_rate=lnr)
    model = Sequential()
    model.add(VGG16(weights='imagenet', include_top=False, input_shape=(64, 64, 3)))
    model.add(Flatten())
    model.add(Dense(batch, activation='relu'))
    model.add(Dense(classes, activation='softmax'))

    model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])

    earlystopping = callbacks.EarlyStopping(monitor="val_loss", mode="min", patience=5, restore_best_weights = True)

    history = model.fit(train, validation_data=val, epochs=epochs, shuffle=True, verbose=1, callbacks = [earlystopping])

    model.save(args.output)