import sys
sys.path.insert(0, '../')
from utils_cv.predict import *
from tensorflow import keras
import numpy 
model = keras.models.load_model("../Data/model.h5")

lettre, p = (predict(model, "../Data/img.png"))
assert type(lettre) == numpy.int64
assert type(p) == numpy.float32

