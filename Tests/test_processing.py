import sys
sys.path.insert(0, '../')
from utils_cv.process_image import *

#Selon l'ordinateur, il faudra éventuellement changer le chemin ci-dessous
def test_processing():
    img = get_image("../Data/img.png")
    img_treated  = processing(img)

    assert img_treated.shape == (1,64,64,3)
   
