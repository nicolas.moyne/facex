# Imports

from utils_cv.webcam import capture_sans_bouton as capture
from utils_cv.predict import predict
from utils_cv.write_file import *
import time
import cv2
from tensorflow import keras


# coding: utf-8
import tkinter as tk
from tkinter import *
from tkinter import ttk
from PIL import ImageTk, Image

###########


# Chemin de l'image qui va servir de stockage lors d'analyse de photos
path_img = "Data/img.png"

# On charge le modèle de reconnaissance visuelle
model = keras.models.load_model("./Data/model.h5")


# On définit un dictionnaire: il va permettre de retrouver
#  une lettre à partir de son numéro
translation = {0: "a", 1: "b", 2: "c", 3: "d", 4: "e", 5: "f", 6: "g", 7: "h", 8: "i", 9: "j", 10: "k", 11: "l", 12: "m", 13: "n",
               14: "o", 15: "p", 16: "q", 17: "r", 18: "s", 19: "t", 20: "u", 21: "v", 22: "w", 23: "x", 24: "y", 25: "z", 26: "DEL", 27: "", 28: " "}


# Interface graphique


# Definition de la fenetre

fenetre = Tk()
fenetre.title("Langage des signes")

# Définition des frames

frame_entiere = ttk.Frame(fenetre, style="Sample.TFrame")
frame_image = ttk.Frame(frame_entiere, style="Sample.TFrame")
frame_boutons = ttk.Frame(frame_entiere, style="Sample.TFrame")
frame_choix = ttk.Frame(frame_entiere, style="Sample.TFrame")


# Vidéo

# on définit un flux vidéo (de la webcam)
cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
frame_image.bind('<Escape>', lambda e: fenetre.quit())
lmain = tk.Label(frame_image)
lmain.pack()


prev_char = None
curr_char = None


def show_frame_and_update():
    global prev_char
    global curr_char

    # update lettre
    capture(path_img, cap)
    caract, proba = predict(model, path_img)

    prev_char = curr_char
    curr_char = caract

    if curr_char == 27 and prev_char != 27:

        c = prev_char

        if c == 26 and texte_entre['text'] != "Texte par défaut":
            texte_entre['text'] = texte_entre['text'][:(
                len(texte_entre['text']) - 1)]

        # pour ajouter un caractere, espace, nothing
        else:
            if texte_entre['text'] == "Texte par défaut":
                texte_entre['text'] = translation[c]
            else:
                texte_entre['text'] = texte_entre['text'] + translation[c]

    # show frame
    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    img = Image.fromarray(cv2image)
    imgtk = ImageTk.PhotoImage(image=img)
    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)
    lmain.after(10, show_frame_and_update)


# Definition des commandes
def enregistre():
    """Fonction qui va s'exécuter lorsque l'utilisateur appuie sur enregistrer"""

    # on prend une capture de la webcam
    capture(path_img, cap)

    # Résultats fournis par le modèle: le caractère le plus probable et sa probabilité
    caract, proba = predict(model, path_img)

    # s'il s'agit du caractère spécial de suppression:
    if caract == 26 and texte_entre['text'] != "Texte par défaut":
        # on supprime la dernière lettre
        texte_entre['text'] = texte_entre['text'][:(
            len(texte_entre['text']) - 1)]

    # pour ajouter un caractere,ou espace, ou nothing
    # on met à jour le texte
    else:
        if texte_entre['text'] == "Texte par défaut":
            texte_entre['text'] = translation[caract]
        else:
            texte_entre['text'] = texte_entre['text'] + translation[caract]


def exporte():
    """Fonction qui va s'exécute lorsque l'utilisateur appuie sur exporter"""

    """Elle va enregistrer le texte dans un fichier texte"""

    # on récupère le chemin
    url = texte.get().strip()

    # on récupère le texte à écrire
    chaine = texte_entre['text']

    # on modifie le fichier
    write_file(url, chaine)


# Définition des boutons

quitter = Button(frame_choix, text="Fermer", command=fenetre.quit)
enrg = Button(frame_boutons, text="Enregister", command=enregistre)
export = Button(frame_boutons, text="Exporter", command=exporte)

# Definition image

img = ImageTk.PhotoImage(Image.open(path_img))

# Definition des label
texte_entre = Label(frame_choix, text="Texte par défaut")


# Definition de l'url
texte = StringVar()
texte.set("Entrez ici l'url du fichier ou stocker le texte")
url_ent = Entry(frame_choix, textvariable=texte, width=30)


# Définition des grilles
enrg.grid(row=0, column=0)
export.grid(row=0, column=1)

texte_entre.grid(row=0, column=0)
url_ent.grid(row=1, column=0)
quitter.grid(row=2, column=0)

frame_image.grid(row=2)
frame_boutons.grid(row=0)
frame_choix.grid(row=1)

# On pack les frames

frame_entiere.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

show_frame_and_update()
fenetre.mainloop()
