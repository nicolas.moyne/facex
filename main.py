
# Imports

from utils_cv.webcam import capture_sans_bouton as capture
from utils_cv.predict import predict
from utils_cv.process_image import crop
import cv2
from tensorflow import keras

# coding: utf-8

# La classe Tkinter Photoimage reconnait les images GIF,PPM .....
import tkinter as tk
from tkinter import *
from tkinter import ttk

# importation des modules utiles dans Pil
from PIL import ImageTk, Image
from utils_cv.ecriture import write_docx as writemf
from utils_cv.handtrack import track_on_img
from platform import system

# Chemin de l'image qui va servir de stockage lors d'analyse de photos

path_img = "./Data/img.png"

# On charge le modèle de reconnaissance visuelle

model = keras.models.load_model("./Data/model.h5")
translation = {0 :"a", 1 : "b", 2 : "c", 3 : "d", 4 : "e", 5 : "f", 6 : "g", 7 : "h", 8 : "i", 9 : "j", 10 : "k", 11 : "l", 12 : "m", 13 : "n", 14 : "o", 15 : "p", 16 : "q", 17 : "r", 18 : "s", 19 : "t", 20 : "u", 21 : "v", 22 : "w", 23 : "x", 24 : "y", 25 : "z", 26 : "DEL", 27 : "", 28 : " "}

if system() == 'Windows':
    cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)  # Fix windows
else:
    cap = cv2.VideoCapture(0)

prev_char = 27 #caractère précédent
curr_char = 27 # caractère actuel


# Definition de la fenetre

fenetre = Tk()
fenetre.title("Langage des signes")

# Définition des frames

frame_entiere = ttk.Frame(fenetre, style="Sample.TFrame")
frame_image = ttk.Frame(frame_entiere, style="Sample.TFrame")
frame_boutons = ttk.Frame(frame_entiere, style="Sample.TFrame")
frame_choix = ttk.Frame(frame_entiere, style="Sample.TFrame")




frame_image.bind('<Escape>', lambda e: fenetre.quit()) # fermer la fenêtre à travers échap

lmain = tk.Label(frame_image) #ajoute un label tkinter

lsecond = tk.Label(frame_image)





# Définition des fonctions d'affichage et de modification


def show_frame_and_update():
    """fonction qui actualise l'image renvoyée par la webcam toutes les 20ms ce qui crée la vidéo, 
    qui définit le mode autoet qui écrit la lettre reconnue""" 
    global prev_char
    global curr_char

    # update lettre

    capture(path_img, cap)  # Prend une capture photo de la webcam
    
    caract, proba = predict(model, path_img) # Retourne le numéro du caractère et sa probabilité

    prev_char = curr_char
    curr_char = caract

    proba = int(100*proba)

    caractere_courant['text'] = "Caractère courant : " + \
        translation[curr_char] + " : (" + str(proba) + "%)"

    if auto: # en cas d'exécution du mode automatique

        if curr_char == 27 and prev_char != 27: # quand le caractère actuel est vide et le caractère précédent ne l'est pas

            c = prev_char
            impr_lettre(c)
            
    # show frame

    _, frame = cap.read()
    frame = cv2.flip(frame, 1)
    # on convertit en format RGB: Par défaut, OpenCV utilise en effet le format BGR
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
    cv2image_crop = crop(cv2image)  # coupe l'image de la main

    img = Image.fromarray(cv2image_crop)  # former un array à partir de l'image

    # création d'une image compatible Tkinter
    imgtk = ImageTk.PhotoImage(image=img)

    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)

    # on met l'image à la bonne taille
    x, y, _ = cv2image.shape
    xp = int((630*x)/y)
    cv2image = cv2.resize(cv2image, (xp, 360))
    cv2image = track_on_img(cv2image)

    img_sec = Image.fromarray(cv2image)  # former un array à partir de l'image

    # création d'une image compatible Tkinter
    imgtk_sec = ImageTk.PhotoImage(image=img_sec)

    lsecond.imgtk_sec = imgtk_sec
    lsecond.configure(image=imgtk_sec)

    lmain.after(20, show_frame_and_update) # auto-appel après 20 ms


def impr_lettre(c):
    """fonction qui permet d'écrire le caractère courant"""
    # si on supprime le dernier caractère 
    if c== 26 and texte_entre['text'] != "Texte par défaut":
        texte_entre['text'] = texte_entre['text'][:(len(texte_entre['text']) - 1)]

    # pour ajouter un caractere ou un espace
    else:
        if texte_entre['text'] == "Texte par défaut":
            texte_entre['text'] = translation[c]
        else:
            texte_entre['text'] = texte_entre['text'] + translation[c]




# Definition des commandes
def enregistre():
    """définit l'action du bouton enregistrer,
    on ajoute le caractère courant à ce qu'on écrit"""
    if not auto:
        capture(path_img, cap)
        caract, proba = predict(model, path_img)
        impr_lettre(caract)


auto = False


def exporte():
    """Définit l'action du bouton exporter,
    on enregitre la chaîne de carctères obtenues dans un fichier .txt"""
    url = texte.get().strip()
    chaine = texte_entre['text']
    writemf(url, chaine)


def change_ability():
    """Définit l'action du bouton Auto ON/OFF"""
    global auto
    auto = not auto
    if auto:
        label_auto['text'] = "AUTO"
    else:
        label_auto['text'] = "MANUEL"


def reset(): 
    """Définit l'action du bouton reset,
    réinitialiser le champ du texte"""
    texte_entre['text'] = "Texte par défaut"





## Définition des boutons

enrg = Button(frame_boutons, text="Enregister", command=enregistre)
export = Button(frame_boutons, text="Exporter", command=exporte)
enabl_disabl = Button(frame_boutons, text="Auto ON/OFF",
                      command=change_ability)
reset_button = Button(frame_boutons, text="Reset", command=reset)
# Definition image
try:
    img = ImageTk.PhotoImage(Image.open(path_img))
except:
    img = ImageTk.PhotoImage(Image.open("Data/blanc.png"))

# Definition des label
texte_entre = Label(frame_choix, text="Texte par défaut")
caractere_courant = Label(frame_choix, text="Caractère courant : ")
label_auto = Label(frame_choix, text="MANUEL")


# Definition de l'url
texte = StringVar()
texte.set("Entrez ici l'url du fichier ou stocker le texte")
url_ent = Entry(frame_choix, textvariable=texte, width=30)


# Définition des grilles
enrg.grid(row=0, column=0)
export.grid(row=0, column=1)
enabl_disabl.grid(row=0, column=2)
reset_button.grid(row=0, column=3)

texte_entre.grid(row=0, column=0)
caractere_courant.grid(row=0, column=1)
url_ent.grid(row=1, column=0)
label_auto.grid(row=1, column=1)


lsecond.grid(row=0, column=0)
lmain.grid(row=0, column=1)

frame_image.grid(row=1)
frame_boutons.grid(row=0)
frame_choix.grid(row=2)

## On pack les frames

frame_entiere.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

## Lancement de l'interface
show_frame_and_update()
fenetre.mainloop()
