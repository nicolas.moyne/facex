 # Organisation de la conception du projet
 
 ## Sprint 0 : Installation des modules

* Choix des modules utiles
* Installation du socle technique
 ## Sprint 1 : Prise en main du réseau de neurones

* **Fonctionnalité 1 :** apprendre à utiliser le réseau de neurone  

* **Fonctionnalité 2:** Traiter les images pour qu'elles soient dans le format correspondant au modèle qui va être utilisé

 * **Fonctionnalité 3 :** Reconnaître un signe à partir d'une photo


## Sprint 2 : Entraînements supplémentaires du modèle pour plus de précision

 *  **Fonctionnalité 4 :** Trouver un dataset de signes ASL

 * **Fonctionnalité 5:** Coder un parser pour pouvoir entraîner un modèle déjà pré-entraîné grâce à des arguments passés en ligne de commande

 * **Fonctionnalité 6:** Entraîner le modèle VGG-16 sur plusieurs époques
 

## Sprint 3 : Implémentation d'une interface graphique avec Tkinter

 * **Fonctionnalité 7:** Prise en main de Tkinter

 * **Fonctionnalité 8:** Création d'une fenêtre simple qui ouvre une image de main, détecte la lettre avec l'IA et l'affiche

 * **Fonctionnalité 9:** Coder la prise de photos avec le webcam grâce à un code python

 * **Fonctionnalité 10:** Intégration de la prise de photos à l'interface graphique

A ce stade, nous avons atteint notre MVP.

 ## Sprint 4 : Ajout de fonctionnalités plus évoluées sur l'interface
 * **Fonctionnalité 11:** : Ecrire une chaîne de caractère donnée dans un fichier .txt ou .docx

 * **Fonctionnalité 12:** Intégrer à l'interface graphique la fonctionnalité d'export d'une chaîne de caractère vers un fichier .txt ou .docx 

 * **Fonctionnalité 13:** Capurer un flux vidéo de la caméra

 * **Fonctionnalité 14:** Visualiser sur l'interface graphique le flux vidéo venant de la caméra, reconnaître des signes de l'ASL sur la vidéo et les afficher.

