# Organisation

* ### Créer le modèle et entraîner le réseau de neurones :

    * Marc-Antoine et Ala  

* ### Regarder commentn enregistrer le modèle :

    * Marc-Antoine et Ala  


* ### Récuperer et afficher la vidéo de la webcam: 

    * Juliette et Nicolas  


* ### Interface graphique : 

    * Nicolas  


* ### Écrire une fonction qui écrit dans un fichier texte :

    * Eya  


* ### Écrire une fonction qui met l'image dans le format demandé :

    * Gabrielle  
---
 **Toutes les fonctions ont été utilisées dans le main** 

---

## Comment le programme fonctionne:
* Capture d'image de la webcam : affiche l'image de la webcam : capture() et capture_sans_bouton()

* Traiter l'image pour qu'elle soit dans le bon format afin d'être passée dans le modèle : processing(img) et retourne une image traitée

* Prédire l'image : predict() renvoie un caractère et la probabilité que ce soit le bon caractère

* Écrire la lettre dans un fichier texte .txt ou .docx :
fonction: write_txt(path_file, chaine) et write_docx(path_file, chaine)
