import cv2
import tensorflow as tf
import mediapipe as mp


# une fonction utile pour pouvoir faire des tests: lit une image
def get_image(path):
    image = cv2.imread(path)
    return image


# une fonction pour mettre l'image dans le bon format
#  pour qu'elle puisse être traitée par le modèle

def processing(image):
    #on convertit en format RGB: Par défaut, OpenCV utilise en effet le format BGR
    img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    #on met l'image à la bonne taille
    img = cv2.resize(img, (64,64))
    img = img.reshape((1, img.shape[0], img.shape[1], img.shape[2]))

    #on utilise la fonction de pre-processing spécifique à VGG16
    img = tf.keras.applications.mobilenet.preprocess_input(img)
    return img


cap = cv2.VideoCapture(0) # capture de la video
 
mpHands = mp.solutions.hands
hands = mpHands.Hands(static_image_mode=False,
                      max_num_hands=2,
                      min_detection_confidence=0.5,
                      min_tracking_confidence=0.5)
mpDraw = mp.solutions.drawing_utils



def crop(img):
    """Coupe l'image de la main"""
    """" prend en paramètre l'image """
    y, x, _ = img.shape

    delta_x, delta_y = 0.05*x, 0.05*y 

    imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    results = hands.process(imgRGB) 

    #initialisations des positions extrêmes des points caractéristiques de la main (Hand Landmark)
    max_x = 0
    min_x = 10000

    max_y = 0
    min_y = 10000

    if results.multi_hand_landmarks:
        for handLms in results.multi_hand_landmarks:
            for lm in handLms.landmark:
                h, w, c = img.shape
                cx, cy = int(lm.x *w), int(lm.y*h)
                max_x = max(max_x, cx)
                min_x = min(min_x, cx)
                max_y = max(max_y, cy)
                min_y = min(min_y, cy) # emplacements extrêmes de ces points 
    
    min_y = int(max(min_y - delta_y, 0))
    min_x = int(max(min_x - delta_x, 0))

    max_y = int(min(max_y + delta_y, y))
    max_x = int(min(max_x + delta_x, x))

    largeur = max(max_x - min_x, max_y - min_y)

    largeur = min(largeur, x - min_x, y-min_y) # largeur de l'image de la main
                
    crop_img = img[min_y:(min_y + largeur), min_x:(min_x + largeur)] # obtenir image de la main carrée 

    if crop_img.shape == (0, 0, _):
        crop_img = cv2.imread("Data/blanc.png") # retourne un blanc si on ne détecte pas d'images

    crop_img = cv2.resize(crop_img, (360,360))
    
    return crop_img # retourne une image carrée de la main 

def process_crop(img):
    return processing(crop(img))
