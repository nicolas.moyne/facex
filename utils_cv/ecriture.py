from docx import Document

def write_txt(path_file, chaine):
    """écrit une chaîne de caractère dans un fichier .txt et l'enregitre dans le dossier désigné par path_file"""
    file = open(path_file, 'w')
    file.write(chaine)
    file.close()

def write_docx(path_file, chaine):
    """écrit une chaîne de caractère dans un fichier .docx et l'enregitre dans le dossier désigné par path_file"""
    document = Document()
    document.add_heading('Ecrit en utilisant ASL-IA', 0)
    p = document.add_paragraph("Le texte : ")
    p.add_run(chaine)
    document.save(path_file)