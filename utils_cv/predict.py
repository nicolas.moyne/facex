###### Imports
from utils_cv.process_image import *
from tensorflow import keras
import numpy as np
######


def predict(model, path_image): 
    """Prédit le numéro du caractère que la main montre en langue des signes"""
    
    """Prend en argument le modèle et le chemin de l'image à analyser"""
    """Retourne le numéro du caractère et sa probabilité"""
    try:
        tab_prediction = model.predict(process_crop(get_image(path_image)), verbose=0) # renvoie la classe prédite de l'image (loaded and resized) , à vérifier avec le modèle
        pred = np.argmax(tab_prediction, axis=1)

        #on accède à la probabilité
        probability = tab_prediction[0][pred[0]]
        return (pred[0],probability)
    except:
        return (27, 1)

