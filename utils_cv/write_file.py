def write_file(path_file, chain):
    """Fonction qui permet d'écrire dans un fichier"""
    """path_file est le chemin du fichier dans lequel on va écrire"""
    """et chain est ce que qu'on veut écrire"""

    #on ouvre le fichier
    file = open(path_file, 'w')

    #on écrit dans la fichier
    file.write(chain)

    #on ferme le fichier
    file.close()