###### Imports
from PIL.Image import WEB
import cv2

######

def capture_sans_bouton(name, cam):
    """Fonction qui prend simplement une capture de la webcam quand on l'appelle"""
    
    ret, frame =cam.read()
    cv2.imwrite(name,frame)

    
def capture_avec_bouton(name):
    """" Prend une capture photo de la webcam"""
    """Première version de la fonctionnalité"""

    """Prend en paramètre le chemin (se finissant par .png) où"""
    """     la photo va être stockée"""

    """Une fenêtre spécifique va s'afficher"""
    
    cam=cv2.VideoCapture(0)
    while True:
        ret, frame = cam.read()
        x , y, c = frame.shape
        frame = cv2.flip(frame, 1)
        cv2.imshow("Output", frame)

        #si l'on appuie sur q, on quitte
        if cv2.waitKey(1) == ord('q') :
            cam.release()
            cv2.destroyAllWindows()
            break
        #si l'on appuie sur a, on prend une photo
        elif cv2.waitKey(1) == ord('a') : 
            cv2.imwrite(name, frame)
            






